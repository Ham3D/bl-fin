
## BI test assignment, Financial system  
  
### Problem:  
You need to implement a backend application that would allow users to transfer funds between wallets. On every transaction, the system will need to take a commission of 1.5%. The application must have the following functionalities:  
- A relational database schema.  
- Upon application start the database should be populated with sample data (e.g several wallets that can be used to transfer funds between)  
- A REST endpoint that can be used to transfer funds  
  
### Solution:  
- there is no auth system  
- I used Single method Controller  
- There are three models:  
- Transaction  
- User  
- Wallet  
- There is a System wallet  
- Each fund transfer creates two Transaction records:  
- one for storing the Fund transferred between users  
- one for storing the transfer between the commission payer and system wallet  
- There is a security check for wallet data consistency  
  
### How to use:  
``` php artisan migrate --seed ```  
  
send a post request to this address:  
``` api/transfer ```  
```  
'sender' => ['required', 'exists:users,id'],  
'receiver' => ['required', 'exists:users,id'],  
'amount' => ['required', 'numeric'],  
'commission_payer' => [Rule::in(Transaction::COMMISSION_PAYER_OPTIONS)],  
```  
commission_payer is not mandatory but can be :  
- senderPaysCommission  
- receiverPaysCommission  
  
#### Tests:  
- there is 4 Feature Test to test APIs  
- there are 5 unit test