<?php

namespace Database\Seeders;

use Exception;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Testing\WithFaker;

class WalletSeeder extends Seeder
{
    use WithFaker;
    
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        // generate system Wallet
        $systemWallet         = new Wallet;
        $systemWallet->id     = 0;
        $systemWallet->amount = 0;
        $systemWallet->type   = Wallet::SYSTEM_WALLET;
        $systemWallet->save();
        
        $users = User::all();
        foreach ($users as $user) {
            $wallet          = new Wallet;
            $wallet->user_id = $user->id;
            $wallet->amount  = random_int(0, 100000);
            $wallet->type    = Wallet::USER_WALLET;
            $wallet->save();
        }
        
    }
}
