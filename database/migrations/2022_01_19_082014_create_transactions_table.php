<?php

use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('transactions', static function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->foreignIdFor(Wallet::class, 'sender')->constrained('wallets');
            $table->foreignIdFor(Wallet::class, 'receiver')->constrained('wallets');
            $table->unsignedBigInteger('amount')->default(0);
            $table->enum('type', Transaction::Types);
            $table->foreignIdFor(Transaction::class, 'related_transaction')->nullable()->constrained('transactions');
            $table->json('comments')->nullable(); // store both user and staff comments
            $table->json('extra')->nullable();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('transaction');
    }
}
