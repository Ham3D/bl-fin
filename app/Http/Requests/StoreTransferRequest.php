<?php

namespace App\Http\Requests;

use App\Models\Transaction;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'sender'           => ['required', 'exists:users,id'],
            'receiver'         => ['required', 'exists:users,id'],
            'amount'           => ['required', 'numeric'],
            'commission_payer' => [Rule::in(Transaction::COMMISSION_PAYER_OPTIONS)],
        ];
    }
}
