<?php

namespace App\Http\Controllers;

use Throwable;
use App\Services\TransferService;
use App\Exceptions\UnknownException;
use App\Http\Requests\StoreTransferRequest;
use Flugg\Responder\Http\Responses\ErrorResponseBuilder;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;

class TransferController extends Controller
{
    /**
     * @param  StoreTransferRequest  $request
     * @param  TransferService       $transferService
     *
     * @return ErrorResponseBuilder|SuccessResponseBuilder
     * @throws UnknownException
     * @throws Throwable
     */
    public function __invoke(StoreTransferRequest $request, TransferService $transferService): ErrorResponseBuilder|SuccessResponseBuilder
    {
        return $transferService->storeTransfer($request);
    }
}
