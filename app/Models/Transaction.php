<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @mixin IdeHelperTransaction
 */
class Transaction extends Model
{
    use HasFactory;
    
    public const FEE = 1.5;
    public const DEFAULT_COMMISSION_PAYER = self::RECEIVER_PAY_COMMISSION;
    public const TRANSFER_FUNDS = 'transferFunds';
    public const COMMISSION = 'transferCommissions';
    
    public const Types = [
        self::TRANSFER_FUNDS,
        self::COMMISSION,
    ];
    
    public const SENDER_PAY_COMMISSION = 'senderPaysCommission';
    public const RECEIVER_PAY_COMMISSION = 'receiverPaysCommission';
    
    public const COMMISSION_PAYER_OPTIONS = [
        self::SENDER_PAY_COMMISSION,
        self::RECEIVER_PAY_COMMISSION,
    ];
    
    protected $casts = [
        'extra'    => 'array',
        'comments' => 'array',
    ];
    
    public function senderWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class, 'sender');
    }
    
    public function receiverWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class, 'receiver');
    }
    
    
}
