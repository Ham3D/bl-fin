<?php

namespace App\Models;

use App\traits\SafetyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * @mixin IdeHelperWallet
 */
class Wallet extends Model
{
    use HasFactory, SafetyTrait;
    
    public const USER_WALLET = 'userWallet';
    public const SYSTEM_WALLET = 'systemWallet';
    public const TYPES = [
        self::USER_WALLET,
        self::SYSTEM_WALLET,
    ];
    
    protected $fillable = ['amount'];
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    
}
