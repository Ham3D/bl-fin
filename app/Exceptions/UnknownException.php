<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class UnknownException extends Exception
{
    public function render(): JsonResponse
    {
        return responder()->error('UNKNOWN_ERROR', 'unknown error happened!')->respond(500);
    }
}
