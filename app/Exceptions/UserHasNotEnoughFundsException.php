<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class UserHasNotEnoughFundsException extends Exception
{
    public function render(): JsonResponse
    {
        return responder()->error('NOT_ENOUGH_FUND', 'user dont has enough fund!')->respond(400);
    }
}
