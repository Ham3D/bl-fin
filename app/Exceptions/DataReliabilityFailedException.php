<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class DataReliabilityFailedException extends Exception
{
    public function render(): JsonResponse
    {
        return responder()->error('DATA_RELIABILITY_FAILED', 'Data reliability is failed, data is tempered!')->respond(500);
    }
}
