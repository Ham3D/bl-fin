<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class WalletNotFoundException extends Exception
{
    public function render(): JsonResponse
    {
        return responder()->error('WALLET_NOT_FOUND', 'wallet not found')->respond(404);
    }
}
