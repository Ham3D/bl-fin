<?php namespace App\Services;

use Exception;
use App\Models\Wallet;
use App\Exceptions\WalletNotFoundException;

class WalletService
{
    /**
     * @throws WalletNotFoundException
     */
    public static function getUserWallet(int $userId): Wallet
    {
        try{
            return Wallet::whereUserId($userId)->firstOrFail();
        }catch (Exception $e){
            throw new WalletNotFoundException();
        }
    }
    
    /**
     * @param            $walletType
     * @param  int|null  $userId
     * @param  int|null  $amount
     *
     * @return void
     * @internal
     */
    public static function generateWallet($walletType, int $userId = null, int $amount = null): void
    {
        $newWallet = new Wallet;
        
        if ($walletType !== null) {
            $newWallet->type = $walletType;
        }
        
        if ($userId !== null) {
            $newWallet->user_id = $userId;
        }
        
        if ($amount !== null) {
            $newWallet->amount = $amount;
        }
        
        $newWallet->save();
    }
    
    /**
     * get system wallet, or generate one uf there isn't any
     * @return Wallet
     */
    public static function getSystemWallet(): Wallet
    {
        $wallet = Wallet::whereType(Wallet::SYSTEM_WALLET)->first();
        
        if ($wallet === null) {
            $wallet         = new Wallet;
            $wallet->amount = 0;
            $wallet->type   = Wallet::SYSTEM_WALLET;
            $wallet->save();
        }
        
        return $wallet;
    }
    
}