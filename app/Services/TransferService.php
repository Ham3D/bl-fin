<?php namespace App\Services;

use Throwable;
use App\Models\Wallet;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use App\Exceptions\UnknownException;
use App\Http\Requests\StoreTransferRequest;
use App\Exceptions\WalletNotFoundException;
use App\Exceptions\UserHasNotEnoughFundsException;
use Flugg\Responder\Http\Responses\ErrorResponseBuilder;
use Flugg\Responder\Http\Responses\SuccessResponseBuilder;

class TransferService
{
    private Wallet $senderWallet;
    private Wallet $receiverWallet;
    private string $commissionPayer;
    
    /**
     * @throws UnknownException
     * @throws Throwable
     */
    public function storeTransfer(StoreTransferRequest $request): ErrorResponseBuilder|SuccessResponseBuilder
    {
        return DB::transaction(function () use ($request) {
            $sender   = $request->get('sender');
            $receiver = $request->get('receiver');
            $amount   = $request->get('amount');
            
            $this->senderWallet    = WalletService::getUserWallet($sender);
            $this->receiverWallet  = WalletService::getUserWallet($receiver);
            $this->commissionPayer = $this->getCommissionPayer($request->get('commission_payer'));
            
            $this->checkIfUserHaveEnoughFunds($amount);
            $this->storeFundTransferTransaction($amount);
            $this->storeCommissionTransaction($amount, $sender, $receiver);
            $this->updateSenderWallet($amount);
            $this->updateReceiverWallet($amount);
            $this->updateSystemWallet($amount);
            
            return responder()->success();
        });
    }
    
    public function storeFundTransferTransaction(int $amount): void
    {
        $newTransaction           = new Transaction;
        $newTransaction->sender   = $this->senderWallet->id;
        $newTransaction->receiver = $this->receiverWallet->id;
        $newTransaction->amount   = $amount;
        $newTransaction->type     = Transaction::TRANSFER_FUNDS;
        $newTransaction->extra    = ['commission_fee' => Transaction::FEE];
        $newTransaction->save();
    }
    
    /**
     * @throws WalletNotFoundException
     */
    public function storeCommissionTransaction(int $amount, int $sender, int $receiver): void
    {
        $newTransaction = new Transaction;
        
        if ($this->commissionPayer === Transaction::SENDER_PAY_COMMISSION) {
            $senderWalletId = WalletService::getUserWallet($sender)->id;
        } else {
            $senderWalletId = WalletService::getUserWallet($receiver)->id;
        }
        $newTransaction->sender   = $senderWalletId;
        $newTransaction->receiver = WalletService::getSystemWallet()->id; // system wallet
        $newTransaction->type     = Transaction::COMMISSION;
        $newTransaction->amount   = $this->calculateCommission($amount);
        $newTransaction->extra    = ['commission_fee' => Transaction::FEE];
        $newTransaction->save();
    }
    
    
    /**
     * @param  int  $amount
     *
     * @return void
     * @throws UserHasNotEnoughFundsException
     */
    public function checkIfUserHaveEnoughFunds(int $amount): void
    {
        $requiredAmount = $this->senderRequiredAmount($amount);
        
        if ($requiredAmount > $this->senderWallet->amount) {
            throw new UserHasNotEnoughFundsException("needed amount: $requiredAmount");
        }
    }
    
    public function getCommissionPayer(string $commissionPayer = null): string
    {
        return $commissionPayer ?? Transaction::DEFAULT_COMMISSION_PAYER;
    }
    
    
    public function calculateCommission(int $amount): int
    {
        return (int) round($amount * Transaction::FEE / 100);
    }
    
    /**
     * @param  int  $amount
     *
     * @return int
     */
    public function senderRequiredAmount(int $amount): int
    {
        if ($this->commissionPayer === Transaction::SENDER_PAY_COMMISSION) {
            return $amount + $this->calculateCommission($amount);
        }
        
        return $amount;
    }
    
    public function updateSenderWallet(int $amount): void
    {
        if ($this->commissionPayer === Transaction::SENDER_PAY_COMMISSION) {
            $requiredAmount = $amount + $this->calculateCommission($amount);
        } else {
            $requiredAmount = $amount;
        }
        
        $this->senderWallet->decrement('amount', $requiredAmount);
    }
    
    public function updateReceiverWallet(int $amount): void
    {
        if ($this->commissionPayer === Transaction::SENDER_PAY_COMMISSION) {
            $newAmount = $this->receiverWallet->amount + $amount;
        } else {
            $newAmount = $this->receiverWallet->amount + $amount - $this->calculateCommission($amount);
        }
        $this->receiverWallet->update(['amount' => $newAmount]);
    }
    
    public function updateSystemWallet(int $amount): void
    {
        $commission   = $this->calculateCommission($amount);
        $systemWallet = WalletService::getSystemWallet();
        $systemWallet->update(['amount' => $systemWallet->amount + $commission]);
    }
    
    
}