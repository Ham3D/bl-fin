<?php

namespace App\traits;

use App\Models\Wallet;
use App\Exceptions\DataReliabilityFailedException;

trait SafetyTrait
{
    public static function bootSafetyTrait(): void
    {
        self::creating(static function (Wallet $model) {
            $model->crc = self::generateCrc($model);
        });
        
        self::updating(static function ($model) {
            $model->crc = self::generateCrc($model);
        });
        
        self::retrieved(static function (Wallet $model) {
            if (self::generateCrc($model) !== $model->crc) {
                throw new DataReliabilityFailedException();
            }
        });
    }
    
    private static function generateCrc($model): string
    {
        return md5($model->user_id.$model->amount);
    }
    
}