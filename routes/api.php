<?php

use App\Http\Controllers\TransferController;

Route::post('transfer', TransferController::class)->name('transfer.store');