<?php

namespace Tests\Unit;

use App\Models\Transaction;
use PHPUnit\Framework\TestCase;
use App\Services\TransferService;

class TransferTest extends TestCase
{
    protected TransferService $transferService;
    
    public function setUp(): void
    {
        $this->transferService = new TransferService();
    }
    
    /**
     * @test
     */
    public function calculateCommission(): void
    {
        
        $this->assertEquals(2, $this->transferService->calculateCommission(100));
        $this->assertEquals(15, $this->transferService->calculateCommission(1000));
    }
    
    /**
     * @test
     */
    public function itCalculateCommissionPayer(): void
    {
        $payer = $this->transferService->getCommissionPayer(null);
        $this->assertEquals(Transaction::DEFAULT_COMMISSION_PAYER, $payer);
        
        $payer = $this->transferService->getCommissionPayer(Transaction::RECEIVER_PAY_COMMISSION);
        $this->assertEquals(Transaction::RECEIVER_PAY_COMMISSION, $payer);
    }
}
