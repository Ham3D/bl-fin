<?php

namespace Tests\Unit;

use Exception;
use Tests\TestCase;
use App\Models\User;
use App\Models\Wallet;
use App\Services\WalletService;
use App\Exceptions\WalletNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WalletTests extends TestCase
{
    use RefreshDatabase;
    
    protected WalletService $walletService;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->walletService = new WalletService();
    }
    
    /**
     * @test
     */
    public function getUserWalletWhenWeKnowThereIsNone(): void
    {
        $this->expectException(WalletNotFoundException::class);
        $this->walletService::getUserWallet(100);
    }
    
    /**
     * @test
     * @throws Exception
     */
    public function getUserWallet(): void
    {
        $user1 = User::factory()->create();
        
        $fund = random_int(1000, 10000);
        WalletService::generateWallet(Wallet::USER_WALLET, $user1->id, $fund);
        $wallet = $this->walletService::getUserWallet($user1->id);
        
        $this->assertInstanceOf(Wallet::class, $wallet);
        $this->assertEquals($fund, $wallet->amount);
        $this->assertEquals($user1->id, $wallet->user_id);
    }
    
    
    /**
     * @test
     */
    public function itWillAlwaysReturnASystemWallet(): void
    {
        $systemWallet = $this->walletService::getSystemWallet();
        $this->assertEquals(Wallet::SYSTEM_WALLET, $systemWallet->type);
    }
    
}
