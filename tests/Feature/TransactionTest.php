<?php

namespace Tests\Feature;

use Exception;
use Tests\TestCase;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Transaction;
use App\Services\WalletService;
use App\Services\TransferService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Exceptions\DataReliabilityFailedException;

class TransactionTest extends TestCase
{
    use RefreshDatabase;
    
    public function setUp(): void
    {
        parent::setUp();
        // create system wallet
        WalletService::getSystemWallet();
    }
    
    /**
     * @test
     * @throws Exception
     */
    public function aSuccessTransferWhenReceiverPaysCommission(): void
    {
        // create users
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        
        // create wallets
        $user1Funds = random_int(1000, 10000);
        $user2Funds = random_int(1000, 10000);
        WalletService::generateWallet(Wallet::USER_WALLET, $user1->id, $user1Funds);
        WalletService::generateWallet(Wallet::USER_WALLET, $user2->id, $user2Funds);
        
        $howMuch  = random_int(500, 1000);
        $params   = [
            'sender'           => $user1->id,
            'receiver'         => $user2->id,
            'amount'           => $howMuch,
            'commission_payer' => Transaction::RECEIVER_PAY_COMMISSION,
        ];
        $response = $this->post(route('transfer.store'), $params);
        
        $response->assertJson(['success' => true]);
        
        $this->assertDatabaseHas('wallets', [
            'user_id' => $user1->id,
            'amount'  => $user1Funds - $howMuch,
        ]);
        
        $commission = (new TransferService())->calculateCommission($howMuch);
        
        $this->assertDatabaseHas('wallets', [
            'user_id' => $user2->id,
            'amount'  => $user2Funds + $howMuch - $commission,
        ]);
        $systemWallet = WalletService::getSystemWallet();
        $this->assertEquals($commission, $systemWallet->amount);
    }
    
    /**
     * @test
     * @throws Exception
     */
    public function aSuccessTransferWhenSenderPaysCommission(): void
    {
        // create users
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        
        // create wallets
        $user1Funds = random_int(1000, 10000);
        $user2Funds = random_int(1000, 10000);
        WalletService::generateWallet(Wallet::USER_WALLET, $user1->id, $user1Funds);
        WalletService::generateWallet(Wallet::USER_WALLET, $user2->id, $user2Funds);
        
        $howMuch  = random_int(500, 1000);
        $params   = [
            'sender'           => $user1->id,
            'receiver'         => $user2->id,
            'amount'           => $howMuch,
            'commission_payer' => Transaction::SENDER_PAY_COMMISSION,
        ];
        $response = $this->post(route('transfer.store'), $params);
        
        $response->assertJson(['success' => true]);
        
        $commission = (new TransferService())->calculateCommission($howMuch);
        
        $this->assertDatabaseHas('wallets', [
            'user_id' => $user1->id,
            'amount'  => $user1Funds - $howMuch - $commission,
        ]);
        $commission = (new TransferService())->calculateCommission($howMuch);
        
        $this->assertDatabaseHas('wallets', [
            'user_id' => $user2->id,
            'amount'  => $user2Funds + $howMuch,
        ]);
        $systemWallet = WalletService::getSystemWallet();
        $this->assertEquals($commission, $systemWallet->amount);
    }
    
    /**
     *
     * @test
     * @throws Exception
     */
    public function transferFundsWhenThereIsNoEnoughFunds(): void
    {
        // create users
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        
        // create wallets
        $user1Funds = random_int(1000, 10000);
        $user2Funds = random_int(1000, 10000);
        WalletService::generateWallet(Wallet::USER_WALLET, $user1->id, $user1Funds);
        WalletService::generateWallet(Wallet::USER_WALLET, $user2->id, $user2Funds);
        
        $howMuch  = 500000;
        $params   = [
            'sender'           => $user1->id,
            'receiver'         => $user2->id,
            'amount'           => $howMuch,
            'commission_payer' => Transaction::SENDER_PAY_COMMISSION,
        ];
        $response = $this->post(route('transfer.store'), $params);
        $response->assertJson(['status' => 400]);
        $response->assertJson(['error' => ['code' => 'NOT_ENOUGH_FUND']]);
    }
    
    /**
     * when we change wallet data not from our code, this will trigger and error next time we use that data
     * @test
     * @throws Exception
     */
    public function throwAnErrorWhenUserChangesTheWalletDataInABadWay(): void
    {
        $this->expectException(DataReliabilityFailedException::class);
        $user1 = User::factory()->create();
        
        $user1Funds = random_int(1000, 10000);
        WalletService::generateWallet(Wallet::USER_WALLET, $user1->id, $user1Funds);
        
        $userWallet         = Wallet::whereType(Wallet::USER_WALLET)->first();
        $userWallet->amount = 100000;
        $userWallet->saveQuietly();
        $userWallet->refresh();
    }
}
